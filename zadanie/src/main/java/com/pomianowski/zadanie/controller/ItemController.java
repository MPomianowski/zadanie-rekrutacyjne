package com.pomianowski.zadanie.controller;

import com.pomianowski.zadanie.dto.ItemDto;
import com.pomianowski.zadanie.model.Item;
import com.pomianowski.zadanie.service.ItemService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/item")
@AllArgsConstructor
public class ItemController {

    private ItemService itemService;

    @PostMapping
    public ResponseEntity registerItem(@RequestBody ItemDto item) {
        if (!item.getName().equals("")) {
            itemService.save(item);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Wartość pola \"name\" nie może być pusta!!!");
        }
    }

    @GetMapping
    public List<Item> getAll() {
        return itemService.findAll();
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable final Integer id) {
        itemService.delete(id);
    }
}

