package com.pomianowski.zadanie.repository;

import com.pomianowski.zadanie.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
}
