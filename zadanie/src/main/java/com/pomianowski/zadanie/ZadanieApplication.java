package com.pomianowski.zadanie;

import com.pomianowski.zadanie.dto.ItemDto;
import com.pomianowski.zadanie.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZadanieApplication implements CommandLineRunner {

	@Autowired
	private ItemService itemRepository;

	public static void main(String[] args) {
		SpringApplication.run(ZadanieApplication.class, args);
	}

	@Override
	public void run(String... args) {
		for(int i = 1; i <= 10; i++){
			itemRepository.save(new ItemDto("Item numer " + i));
		}
	}
}
