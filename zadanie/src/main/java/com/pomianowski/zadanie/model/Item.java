package com.pomianowski.zadanie.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class Item {

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    Integer id;
    @Column(name = "name")
    String name;
    @Column(name = "creation_at")
    Date createdAt;

    public Item(String name) {
        this.name = name;
    }

}
