package com.pomianowski.zadanie.service;

import com.pomianowski.zadanie.dto.ItemDto;
import com.pomianowski.zadanie.model.Item;
import com.pomianowski.zadanie.repository.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class ItemService {

    private ItemRepository itemRepository;

    public List<Item> findAll(){
        List<Item> items = itemRepository.findAll();
        Collections.shuffle(items);
        return items;
    }

    public void save(ItemDto itemDto) {
        Item item = new Item(itemDto.getName());
        item.setCreatedAt(new Date());
        itemRepository.save(item);
    }

    public void delete(Integer id) {
        Optional<Item> item = itemRepository.findById(id);
        item.ifPresent(value -> itemRepository.delete(value));
    }
}
